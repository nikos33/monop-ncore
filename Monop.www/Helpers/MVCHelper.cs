﻿using System;
using System.Text;
using System.Collections.Generic;
using System.Collections;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;

namespace Monop.www.Helpers
{

    public static class MVCHelper
    {


        public static IHtmlHelper Span(this IHtmlHelper helper, string id) => helper.Span(id, id);
        

        public static IHtmlHelper Span(this IHtmlHelper helper, string id, string defText)
        {
            var text = SessionHelper.GetText(id);
            if (string.IsNullOrEmpty(text)) text = defText;

            var ctx = helper.ViewContext.RequestContext;

            if (ctx.HttpContext.Request.IsAuthenticated)
            {
                var uname = ctx.HttpContext.User.Identity.Name;
                if (ConfigHelper.IsAdmin(uname))
                {
                    if (String.IsNullOrEmpty(text)) text = defText;
                    return HtmlString.Create(
                        String.Format("<span  id=\"{0}\" class=\"trans_element\">{1}</span>", id, text));
                }
            }
            return HtmlString.Create(text);

        }

        public static string Text(this IHtmlHelper helper, string id)
        {
            string tt = SessionHelper.GetText(id);
            return tt ?? id;
        }

        public static string Text(this IHtmlHelper helper, string text_en_en, string text_ru_ru)
        {
            if (SessionHelper.Locale == "en-US")
            {
                return text_en_en;
            }
            else if (SessionHelper.Locale == "ru_RU")
            {
                return text_ru_ru;
            }

            return "no text";

        }

        #region Table

        public static string Table(this IHtmlHelper helper, string name, IList items, IDictionary<string, object> attributes)
        {
            if (items == null || items.Count == 0 || string.IsNullOrEmpty(name))
            {
                return string.Empty;
            }

            return BuildTable(name, items, attributes);
        }

        private static string BuildTable(string name, IList items, IDictionary<string, object> attributes)
        {
            StringBuilder sb = new StringBuilder();
            BuildTableHeader(sb, items[0].GetType());

            foreach (var item in items)
            {
                BuildTableRow(sb, item);
            }

            TagBuilder builder = new TagBuilder("table");
            builder.MergeAttributes(attributes);
            builder.MergeAttribute("name", name);
            builder.InnerHtml = sb.ToString();
            return builder.ToString(TagRenderMode.Normal);
        }

        private static void BuildTableRow(StringBuilder sb, object obj)
        {
            Type objType = obj.GetType();
            sb.AppendLine("\t<tr>");
            foreach (var property in objType.GetProperties())
            {
                sb.AppendFormat("\t\t<td>{0}</td>\n", property.GetValue(obj, null));
            }
            sb.AppendLine("\t</tr>");
        }

        private static void BuildTableHeader(StringBuilder sb, Type p)
        {
            sb.AppendLine("\t<tr>");
            foreach (var property in p.GetProperties())
            {
                sb.AppendFormat("\t\t<th>{0}</th>\n", property.Name);
            }
            sb.AppendLine("\t</tr>");
        }

        #endregion
    }

}
